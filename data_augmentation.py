""" created by Guillaume Barthe and Benjamin PETIT on the 17/03/2021 at 14:57 """
import os
import time
import cv2 as cv
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage


def data_augment(data, label, output, x):
    import imgaug.augmenters as iaa

    yaug = CNNToImgAug(data, label)

    for n in range(x):
        for image, bbs, current_time in yaug:
            # e.g. Sometimes(0.5, GaussianBlur(0.3)) would blur roughly every second image.
            sometimes = lambda aug: iaa.Sometimes(0.6, aug)

            # Define our sequence of augmentation steps that will be applied to every image
            # All augmenters with per_channel=0.5 will sample one value _per image_
            # in 50% of all cases. In all other cases they will sample new values
            # _per channel_.
            seq = iaa.Sequential(  # apply the following augmenters to most images
                [
                    iaa.Fliplr(0.5),  # horizontally flip 50% of all images
                    iaa.Flipud(0.2),  # vertically flip 20% of all images
                    sometimes(iaa.Affine(
                        scale=(0.8, 1.20),  # scale images to 80-120% of their size, individually per axis
                        translate_percent=(-0.2, 0.2),  # translate by -20 to +20 percent (per axis)
                        rotate=(-10, 10),  # rotate by -45 to +45 degrees
                        shear=(-5, 5),  # shear by -16 to +16 degrees
                        order=[0, 1],  # use nearest neighbour or bilinear interpolation (fast)
                    )),
                    iaa.SomeOf((0, 2),
                               # execute 0 to 5 of the following augmenters per image does not execute all of them,
                               # be way too strong
                               [
                                   iaa.OneOf([
                                       iaa.GaussianBlur((0, 0.5)),  # blur images with a sigma between 0 and 3.0
                                       iaa.AverageBlur(k=(2, 3)),
                                       # blur image using local means with kernel sizes between 2 and 7
                                       iaa.MedianBlur(k=(7, 7)),
                                       # blur image using local medians with kernel sizes between 2 and 7
                                   ]),
                                   iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)),  # sharpen images
                                   iaa.Emboss(alpha=(0, 1.0), strength=(0, 1.0)),  # emboss images
                                   # search either for all edges or for directed edges,
                                   # blend the result with the original image using a blobby mask
                                   iaa.SimplexNoiseAlpha(iaa.OneOf([
                                       iaa.EdgeDetect(alpha=(0.5, 1.0)),
                                       iaa.DirectedEdgeDetect(alpha=(0.5, 1.0), direction=(0.0, 1.0)),
                                   ])),
                                   iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.01 * 255), per_channel=0.5),
                                   # # add gaussian noise to images
                                   iaa.OneOf([
                                       iaa.Dropout((0.01, 0.05), per_channel=0.5),
                                       # randomly remove up to X% of the pixels
                                       iaa.CoarseDropout((0.03, 0.15), size_percent=(0.02, 0.05), per_channel=0.2),
                                   ]),
                                   iaa.Invert(0.05, per_channel=True),  # invert color channels
                                   iaa.Add((-10, 10), per_channel=0.5),
                                   # change brightness of images (by -10 to 10 of original value)
                                   iaa.AddToHueAndSaturation((-10, 10)),  # change hue and saturation
                                   iaa.OneOf([
                                       # [either change the brightness of the whole image (sometimes per channel) or
                                       # change the brightness of subareas
                                       iaa.Multiply((0.5, 1.5), per_channel=0.5),
                                       iaa.FrequencyNoiseAlpha(
                                           exponent=(-4, 0),
                                           first=iaa.Multiply((0.5, 1.5), per_channel=True),
                                           second=iaa.LinearContrast((0.5, 2.0))
                                       )
                                   ]),
                                   iaa.LinearContrast((0.1, 0.2), per_channel=0.1),
                                   # improve or worsen the contrast
                                   iaa.Grayscale(alpha=(0.0, 1.0)),
                                   sometimes(iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)),
                                   # # move pixels locally around (with random strengths)
                                   sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.02))),
                                   # sometimes move parts of the image around
                                   # sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1)))
                               ],
                               random_order=True
                               )
                ],
                random_order=True
            )

            # Augment BBs and images.

            image_aug, bbs_aug = seq(image=image, bounding_boxes=bbs)

            exportToCNN(output, image_aug, bbs_aug, dir_warning=False)
        n = n + 1
        # image with BBs before/after augmentation (shown below)
        # image_after = bbs_aug.draw_on_image(image_aug, size=2, color=[0, 0, 255])


def parseXYXY(labels, h, w, c):
    label, xmin, ymin, xmax, ymax = labels.label, labels.x1, labels.y1, labels.x2, labels.y2
    return label, (xmin + xmax) / 2 / w, \
                  (ymin + ymax) / 2 / h, \
                  (xmax - xmin) / w, \
                  (ymax - ymin) / h


class CNNToImgAug:
    image_path_list: list = []
    label_path_list: list = []
    prefix_data: list = []

    def __init__(self, image_path, label_path, *args, **kwargs) -> None:
        self.getData(image_path, label_path, ['.png', '.jpeg', '.jpg'])
        # self.checkErr(image_path, label_path)

    def getData(self, image_path, labels_path, exts):
        image_path_filenames = os.listdir(image_path)
        for filename in image_path_filenames:
            prefix, ext = os.path.splitext(filename)
            if not os.path.exists(os.path.join(labels_path, prefix + '.txt')):
                continue
            if ext.lower() not in exts:
                continue
            self.image_path_list.append(os.path.join(os.path.abspath(image_path), filename))
            self.label_path_list.append(os.path.join(os.path.abspath(labels_path), prefix + '.txt'))
            self.prefix_data.append(prefix)

    def checkErr(self, image_path, label_path):
        list_img_filenames = sorted(os.listdir(image_path))
        list_labels_filenames = sorted(os.listdir(label_path))
        self.prefix_data = []

        for filename in list_img_filenames:
            prefix, _ = os.path.splitext(filename)
            if prefix + '.txt' not in list_labels_filenames:
                self.image_path_list.remove(os.path.join(os.path.abspath(image_path), filename))
            else:
                self.prefix_data.append(prefix)

        for filename in list_labels_filenames:
            prefix, _ = os.path.splitext(filename)
            flag = False
            for test_ex in ['.png', '.jpeg', '.jpg']:
                if prefix + test_ex in list_img_filenames:
                    flag = True
                    break
            if not flag:
                print(filename, os.path.join(os.path.abspath(label_path), filename))
                self.label_path_list.remove(os.path.join(os.path.abspath(label_path), filename))

    # Possible bug quand la liste n'est pas ordonnee
    @staticmethod
    def pathToFilelist(path: str, ext: list):
        return sorted([os.path.join(os.path.abspath(path), i)
                       for i in os.listdir(path)
                       if os.path.splitext(i)[-1].lower() in ext])

    def __len__(self):
        return len(self.image_path_list)

    def __iter__(self):
        self.it = 0
        return self

    def __next__(self):
        if self.it < len(self.image_path_list):
            return self.parse()
        else:
            raise StopIteration

    def parseLabelFile(self):
        labels_path = self.label_path_list[self.it]
        labels_fd = open(labels_path)
        labels = [[float(j) for j in i.split('\n')[0].split()]
                  for i in labels_fd.readlines() if len(i) > 0]
        labels_fd.close()
        return labels

    @staticmethod
    def labelRawToBB(labels, h, w, c):
        bboxs = []
        min_max_coo = lambda center, m, s: [int(m * (center - (s / 2))), int(m * (center + (s / 2)))]

        for it_labels in labels:
            cls, x_center, y_center, width, height = it_labels

            min_x, max_x = min_max_coo(x_center, w, width)
            min_y, max_y = min_max_coo(y_center, h, height)

            bboxs.append(BoundingBox(min_x, min_y, max_x, max_y, int(cls)))

        return BoundingBoxesOnImage(bboxs, (h, w, c))

    def parse(self):
        image_path = self.image_path_list[self.it]
        prefix = self.prefix_data[self.it]
        image = cv.imread(image_path)
        labels_raw = self.parseLabelFile()
        labels = self.labelRawToBB(labels_raw, *image.shape)
        self.it += 1
        return image, labels, prefix


def exportToCNN(export_path: str, image_aug, labels_aug: BoundingBoxesOnImage, dir_warning=True):
    images_paths = os.path.join(os.path.abspath(export_path) + '/images')
    labels_paths = os.path.join(os.path.abspath(export_path) + '/labels')
    try:
        os.makedirs(images_paths)
        os.makedirs(labels_paths)
    except FileExistsError:
        if dir_warning:
            print('Output directory already exists.')
    current_time = time.time()
    cv.imwrite(os.path.join(images_paths, str(current_time) + '.png'), image_aug)
    labels_fd = open(os.path.join(labels_paths, str(current_time) + '.txt'), 'w')

    labels_fd.write('\n'.join(['{} {} {} {} {}'.format(*parseXYXY(i, *image_aug.shape))
                               for i in labels_aug.bounding_boxes]))

    labels_fd.close()

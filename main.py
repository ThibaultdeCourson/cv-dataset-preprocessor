# Base packages
import os
import cv2 as cv
import importlib
import pandas as pd
from time import time
from statistics import mean

# Project packages
from detection_tools import detect_crop_and_detect, item1s_recovery, basis_change, get_output, detect_with_CNN, train_CNN
from default_parameters import params
from common import to_opencv_hsv, prepare_output, shift_coords, measure_duration, complete_dict, display, \
    initialize_dir, to_bgr
from data_conversion import Dataset
from shapes_detection import shapes_detection

if __name__ == '__main__':
    start_time = None
    if params['verbose'] > 0: start_time = time()

    source = params['data_path'] + '/input/'

    """
    TRAINING FIRST MODEL
    """
    weights_file = ('best' if params['CNNs']['state_detection']['use']['best_weights'] else 'last') + '.pt'
    weights = params['CNNs']['state_detection']['weights_path'] + '/weights/' + weights_file
    if not os.path.isfile(weights) or params['CNNs']['state_detection']['train']['overwrite']:
        print("Training first model")

        train_ds = Dataset(source_path=source,
                           task=params['CNNs']['state_detection']['task'],
                           cropping=params['CNNs']['state_detection']['cropping'],
                           params=params['CNNs']['state_detection']['train'],
                           training=True)

        if len(params['CNNs']['state_detection']['train']['splitting_config']) == 1:

            results = []
            for i in range(params['CNNs']['state_detection']['train']['splitting_config'][0]):
                fold_name = 'fold' + str(i + 1)
                root_path = source + '/' + params['CNNs']['state_detection']['train']['item_name']['folder'] #+ train_ds.suffix
                results.append(train_CNN(source_path=root_path + '/' + fold_name + '/',
                                             param=params['CNNs']['state_detection'],
                                         output_path=root_path,
                                         n_objects=params['n_item1s'],
                                         training_name=fold_name))

            metric_values = dict()

            for result in results:
                if result is not None:
                    for metric, metric_val in result.results_dict.items():
                        if metric not in metric_values:
                            metric_values[metric] = []
                        metric_values[metric].append(metric_val)

            if len(metric_values) > 0:
                metric_df = pd.DataFrame.from_dict(metric_values)
                visualize_metric = ['mean', 'std', 'min', 'max']
                print(metric_df.describe().loc[visualize_metric])
    else:
        print("Using model already trained")

    """
    PARAMETERS IMPORTATION
    """

    if os.path.isdir(params['data_path'] + '/input/' + params['source']):
        print("Source [", params['source'], "] selected", sep='')
    else:
        raise ValueError(
            "Error: the source directory " + params['data_path'] + '/input/' + params['source'] + " does not exists")

    if os.path.isfile(params['data_path'] + '/input/' + params['source'] + '/parameters.py'):
        if params['verbose'] > 1: print("\nData parameters file found, parameters ", end='')
        data_parameters = importlib.import_module(params['data_path'] + '/input.' + params['source'] + '.parameters',
                                                  package='parameters')

        if hasattr(data_parameters, 'param'):
            for parameter in data_parameters.param:
                if params['verbose'] > 1: print(parameter + ', ', end='')
                params[parameter] = complete_dict(params, data_parameters.param, parameter)

                # if type(data_parameters.param[parameter]) == dict:
                #     for value in data_parameters.param[parameter]:
                #         param[parameter][value] = data_parameters.param[parameter][value]
                # else:
                #     param[parameter] = data_parameters.param[parameter]
        if params['verbose'] > 1: print(" imported\n")
    else:
        print("No data parameters (input/" + params['source'] +
              "/parameters.py) found, using default parameters")

    # if param['suffix'] != '':
    #     for item in param['item_name']: param['item_name'][item] += param['suffix']
    #     for item in param['labels']: param['labels'][item]['name'] += param['suffix']
    # for item in param['output']:
    #     param['output'][item]['name'] = (param['data_path'] + '/output/' + param['source'] + '/' +
    #                                      param['output'][item]['name'] + param['suffix'])

    """
    INPUT PRE-PROCESSING
    """

    # Check the input and convert it to a list of images if necessary
    dataset = Dataset(source_path=params['data_path'] + '/input/' + params['source'],
                      source_type=params['source_type'],
                      item_name=params['item_name'],
                      fps=params['fps'],
                      start=params['video_cut'][0],
                      end=params['video_cut'][1],
                      cropping=params['cropping'],
                      task=params['task'],
                      overwrite=params['overwrite'])

    params['fps'] = 25 if dataset.fps == -1 else dataset.fps

    # if dataset.suffix != '':
    #     for item in param['item_name']: param['item_name'][item] += dataset.suffix
    #     for parameter in ('output', 'labels'):
    #         for item in param[parameter]: param[parameter][item]['name'] += dataset.suffix

    orientation = None
    if os.path.isfile(params['data_path'] + '/input/' + params['source'] + '/orientation.csv'):
        orientation = pd.read_csv(params['data_path'] + '/input/' + params['source'] + '/orientation.csv', sep=';',
                                  index_col='frame', squeeze=True)

    # results = {'img': 0, 'item2s': 0, 'item1s': 0, 'isolatedw': 0, 'img_with_item2': 0, 'not_enough_item1s': 0,
    #            'too_much_item1s': 0, 'contact_pts': 0, 'img_with_tp': 0, 'newPierre': 0, 'lois_cps': 0, 'shape2': 0,
    #            'shape1': 0, 'shape3': 0, 'img_with_shapes': 0, 'shapes': 0, 'alignment': 0,
    #            'clean_alignment': 0, 'realistic_alignment': 0, 'item1_measure': [0] * 1 if param['n_item1s'] is None else param['n_item1s']}

    # range_colors = None
    # if param['debug'] > 1:
    #     range_colors = {'HSV': {'min': [[256, -1], [256, -1], [256, -1]], 'max': [[-1, -1], [-1, -1], [-1, -1]]},
    #                     'BGR': {'min': [[101, -1], [101, -1], [101, -1]], 'max': [[-1, -1], [-1, -1], [-1, -1]]}}

    if params['complete_data']: last_result = {'target_points': None, 'measures': None, 'time': 0}

    """
    MASKS IMPORTATION
    """

    params['shapes']['shape1']['color'] = to_bgr('salmon')
    shapes = params['shapes'].keys()
    for marking in shapes:
        if params['shapes'][marking]['import_mask']:
            params['shapes'][marking]['mask'] = cv.threshold(
                cv.imread(params['data_path'] + '/input/' + params['source'] + '/mask_' + marking + '.png',
                          cv.IMREAD_GRAYSCALE),
                100, 255, cv.THRESH_BINARY)[1]
        else:
            params['shapes'][marking]['mask'] = None

            # Converts the filter bounds to the OpenCV format
            # if param['debug'] < 1:
            for bound in ('lower', 'upper'):
                params['shapes'][marking][bound] = to_opencv_hsv(params['shapes'][marking][bound])

    """
    PREPARATION OF OUTPUT FILES
    """
    file, video, labels_video = None, None, None
    if params['output']['csv']['activate'] or params['output']['img']['activate'] or params['output']['video']['activate']:
        initialize_dir(params['data_path'] + '/output/' + params['source'], overwrite=False)

    # Cropped frames
    if params['debug'] > 1: initialize_dir(params['data_path'] + '/output/' + params['source'] + '/frames_cropped/')

    # CSV file
    if prepare_output(params['output']['csv']['name'] + '.csv', params['output']['csv']['overwrite'], 'csv',
                      params['output']['csv']['activate']):
        file = open(params['output']['csv']['name'] + '.csv', "w+")  # Output file

        content = "Frame;"
        if params['debug'] > 1: content += "Img Hue;Img Sat;Img Val;Img Blue;Img Green;Img Red"
        file.write(content + ';'.join(['item1 ' + str(i + 1) for i in range(params['n_item1s'])]))
    else:
        params['output']['csv']['activate'] = False

    # Labelled images folder
    if prepare_output(path=params['output']['img']['name'],
                      overwrite=params['output']['img']['overwrite'],
                      output_type='img',
                      activate=params['output']['img']['activate']):
        initialize_dir(params['output']['img']['name'] + '/measure')
        initialize_dir(params['output']['img']['name'] + '/detection')
    else:
        params['output']['img']['activate'] = False

    # Video
    if prepare_output(path=params['output']['video']['name'] + '_labels.mp4',
                      overwrite=params['output']['video']['overwrite'],
                      output_type='video',
                      activate=params['output']['video']['activate']):
        # choose codec according to format needed
        labels_video = cv.VideoWriter(params['output']['video']['name'] + '_labels.mp4',
                                      cv.VideoWriter_fourcc(*'mp4v'), params['fps'] * params['speed'],
                                      params['img_size'] if 'img_size' in params else None)
    else:
        params['output']['video']['activate'] = False

    if prepare_output(params['output']['video']['name'] + '.mp4', params['output']['video']['overwrite'], 'video',
                      params['output']['video']['activate']):
        # choose codec according to format needed
        video = cv.VideoWriter(params['output']['video']['name'] + '.mp4', cv.VideoWriter_fourcc(*'mp4v'),
                               params['fps'] * params['speed'], params['img_size'] if 'img_size' in params else None)
    else:
        params['output']['video']['activate'] = False

    visuals = [None, None, None]

    print('')

    if params['verbose'] > 0:
        measure_duration(start_time, time(), "Initialization duration")
        start_time = time()

    """
    DATA PROCESSING USING CNN
    """

    images = []
    detect_with_CNN(source=params['data_path'] + '/input/' + params['source'],
                    folder=params['item_name']['folder'],
                    param=params['labels']['face'],
                    name='face')

    if params['verbose'] > 0:
        measure_duration(start_time, time(), "item2s detection duration")
        start_time = time()

    """
    MAIN LOOP: ADDITIONAL DATA PROCESSING
    """
    for image in images:
        print(image['number'], ": ", sep='', end='')
        target_points = []
        if params['output']['csv']['activate']: file.write("\n" + str(image['number']) + ";")

        """
        SHAPE DETECTION
        """
        if visuals[0] is not None:
            visuals[1] = image['img'].copy()

        if item1s_recovery(image,
                           params['data_path'] + '/input/' + params['source'] + '/' + params['labels']['item1s'][
                               'name'] + '/labels/' +
                           os.path.splitext(image['filename'])[0] + "_", params['n_item1s'], visuals, results,
                           params['verbose'] > 2):

            if visuals[0] is not None:
                visuals[1] = image['img'].copy()
                if params['verbose'] > 3: display(visuals[0])

            """
            Shape detection to filtrate frames
            """
            # Detect shapes or resize the imported masks
            exclude = [(shift_coords((bBox['left'], bBox['top']), image['shifting'], inverse=True),
                        shift_coords((bBox['right'], bBox['bottom']), image['shifting'], inverse=True))
                       for bBox in image['item1s']]
            shape2s = shapes_detection(image['img'], params['shapes'], image['analysis_zone'], exclude,
                                       visuals[1], results)

            if len(shape2s) < len(shapes):
                print("shapes not detected:", (", ".join(shapes - shape2s)), end='')

            if len(shape2s) > 1:
                results['img_with_shapes'] += 1

            if params['verbose'] > 2: display(visuals[1])

            """
            EXAMPLE OF MEASURE
            """

            # output measuring among item2s not associated to a item1
            for iitem1 in range(len(image['item1s'])):
                if params['verbose'] > 1: print("item1 ", (iitem1 + 1), ": ", sep='')

                outputs = []
                for iitem2 in range(len(image['item1s'][iitem1]['item2s'])):
                    if 'target_point' in image['item1s'][iitem1]['item2s'][iitem2]:
                        if params['verbose'] > 1:
                            print("item2 ", image['item1s'][iitem1]['item2s'][iitem2]['position'] + 1, ": ",
                                  sep='', end='')

                        results['shapes'] += 1

                        basis_change(image['item1s'][iitem1]['item2s'][iitem2],
                                     image['item1s'][iitem1]['shifting'])

                        # Draws the target point on the image
                        if visuals[1] is not None:
                            cv.circle(visuals[1], image['item1s'][iitem1]['item2s'][iitem2]['target_point'], 1,
                                      (255, 255, 255), -1)

                        # Deviation calculation

                        output = get_output(image['item1s'][iitem1]['item2s'][iitem2]['target_point'],
                                            params['shapes'], image['img'].shape, visuals[1], results,
                                            params['verbose'])

                        if output is not None:
                            image['item1s'][iitem1]['item2s'][iitem2]['shape1_output'] = output
                            outputs.append(output)

                            if visuals[2] is not None:
                                cv.circle(visuals[2], image['item1s'][iitem1]['item2s'][iitem2]['target_point'],
                                          radius=2, color=to_bgr('yellow'), thickness=-1)
                                if params['output']['video']['activate']:
                                    cv.putText(visuals[2], str(round(output)) + " cm",
                                               (image['item1s'][iitem1]['item2s'][iitem2]['target_point'][0] + 10,
                                                image['item1s'][iitem1]['item2s'][iitem2]['target_point'][1] + 5),
                                               cv.FONT_HERSHEY_COMPLEX_SMALL, 1, to_bgr('white'), 1,
                                               cv.LINE_AA)

                    elif params['verbose'] > 1:
                        print("no target point found")

                if len(outputs) > 0:
                    image['item1s'][iitem1]['shape1_output'] = mean(outputs)
                    while iitem1 >= len(results['item1_measure']): results['item1_measure'].append(0)
                    results['item1_measure'][iitem1] += 1

            if params['verbose'] > 2: display(visuals[1])

            # Saves the results in a file
            if params['output']['csv']['activate']:
                for iitem1 in range(len(image['item1s'])):
                    if 'shape1_output' in image['item1s'][iitem1]:
                        file.write(str(image['item1s'][iitem1]['shape1_output']))
                    if iitem1 < len(image['item1s']) - 1: file.write(";")

            """
                if param['complete_data']:
                    if all(elem is None for elem in frame_result):
                        if last_result['time'] < param['fps'] / 10 \
                        and last_result['measures'] is not None:
                            for i in range(len(last_result['measures'])):
                                print(str(last_result['measures'][i]) + " cm", last_result['target_points'][i])
                                cv.putText(visuals[1], str(round(last_result['measures'][i])) + " cm",
                                           last_result['target_points'][i],
                                           cv.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255), 1, cv.LINE_AA)
                            last_result['time'] += 1

                    else:
                        last_result['target_points'] = target_points
                        last_result['measures'] = list(filter(lambda measure: measure is not None, frame_result))

                        if last_result['time'] != 0:
                            last_result['time'] = 0
                """

            # Saves the visual
            if params['output']['img']['activate']:
                cv.imwrite(params['output']['img']['name'] + '/measure/' + image['filename'], visuals[1])

        if params['verbose'] > 0: print('')

        """
        OUTPUTS SAVING
        """

        # Saves the visual
        if params['output']['img']['activate']:
            cv.imwrite(params['output']['img']['name'] + '/detection/' + image['filename'], visuals[0])

        # Adds the frame to the video
        if params['output']['video']['activate']:
            labels_video.write(visuals[0])
            video.write(visuals[2])

    if params['verbose'] > 0: measure_duration(start_time, time(), "Processing duration")

    # Finishes writing in the csv file
    if params['output']['csv']['activate']: file.close()

    # Finishes writing in the video
    if params['output']['video']['activate']:
        # cv.destroyAllWindows()
        labels_video.release()
        video.release()

    """
    DISPLAY OF STATISTICS
    """

    # if param['debug'] > 1:
    #     print("Hue: between ", round(range_colors['HSV']['min'][0][0]), "° (frame ", range_colors['HSV']['min'][0][1],
    #           ") and ", round(range_colors['HSV']['max'][0][0]), "° (frame ", range_colors['HSV']['max'][0][1],
    #           ")\nSat: between ", round(range_colors['HSV']['min'][1][0]), "% (frame ",
    #           range_colors['HSV']['min'][1][1],
    #           ") and ", round(range_colors['HSV']['max'][1][0]), "% (frame ", range_colors['HSV']['max'][1][1],
    #           ")\nVal: between ", round(range_colors['HSV']['min'][2][0]), "% (frame ",
    #           range_colors['HSV']['min'][2][1],
    #           ") and ", round(range_colors['HSV']['max'][2][0]), "% (frame ", range_colors['HSV']['max'][2][1], ")\n",
    #           "Blue: between ", round(range_colors['BGR']['min'][0][0]), "° (frame ", range_colors['BGR']['min'][0][1],
    #           ") and ", round(range_colors['BGR']['max'][0][0]), "° (frame ", range_colors['BGR']['max'][0][1],
    #           ")\nGreen: between ", round(range_colors['BGR']['min'][1][0]), "% (frame ",
    #           range_colors['BGR']['min'][1][1],
    #           ") and ", round(range_colors['BGR']['max'][1][0]), "% (frame ", range_colors['BGR']['max'][1][1],
    #           ")\nRed: between ", round(range_colors['BGR']['min'][2][0]), "% (frame ",
    #           range_colors['BGR']['min'][2][1],
    #           ") and ", round(range_colors['BGR']['max'][2][0]), "% (frame ", range_colors['BGR']['max'][2][1], ")\n",
    #           sep='')

    # if param['verbose'] > 0:
    #     n_item1s = param['n_item1s'] * results['img']
    #     print("\n", results['img'], " images analysed with ", param['n_item1s'], " item1s in each:\nSuccess: ",
    #           results['realistic_alignment'], " (", round(100 * results['realistic_alignment'] / n_item1s / 2),
    #           "%) (item2s with successful output measuring)\nMissing item2 bounding boxes: ",
    #           n_item1s * 2 - results['item2s'], "/", n_item1s * 2, " (",
    #           round(100 - 100 * results['item2s'] / n_item1s / 2),
    #           "%)\n   Missing item1 bounding boxes: ", n_item1s - results['item1s'], "/", n_item1s, " (",
    #           round(100 - 100 * results['item1s'] / n_item1s), "%)", sep='')
    #
    #     if results['img_with_item2'] > 0:
    #         print("   Images with not enough item1s identified: ", results['not_enough_item1s'], "/",
    #               results['img_with_item2'], " (",
    #               round(100 * results['not_enough_item1s'] / results['img_with_item2']),
    #               "%)\n   Images with too much item1s identified: ", results['too_much_item1s'], "/",
    #               results['img_with_item2'], " (", round(100 * results['too_much_item1s'] / results['img_with_item2']),
    #               "%)", sep='')
    #
    #         if results['item2s'] > 0:
    #             print("Missing target points: ", results['item2s'] - results['contact_pts'], "/", results['item2s'],
    #                   " (",
    #                   round(100 - 100 * results['contact_pts'] / results['item2s']), "%)", end='', sep='')
    #
    #             if results['img_with_tp'] > 0:
    #                 print(", ", round(100 * results['lois_cps'] / results['contact_pts']),
    #                       "% found using Lois' program\nImages with not enough shapes detected: ",
    #                       results['img_with_tp'] - results['img_with_shapes'], "/", results['img_with_tp'], " (",
    #                       round(100 - 100 * results['img_with_shapes'] / results['img_with_tp']),
    #                       "%)\n   Images with no shape detected: ",
    #                       results['img_with_tp'] - results['shape2'],
    #                       "/", results['img_with_tp'], " (",
    #                       round(100 - 100 * results['shape2'] / results['img_with_tp']),
    #                       "%)\n   Images with no shape1 detected: ",
    #                       results['img_with_tp'] - results['shape1'],
    #                       "/", results['img_with_tp'],
    #                       " (", round(100 - 100 * results['shape1'] / results['img_with_tp']),
    #                       "%)\n   Images with no Stayer shape1 detected: ", results['img_with_tp'] - results['shape3'],
    #                       "/", results['img_with_tp'],
    #                       " (", round(100 - 100 * results['blue_line'] / results['img_with_tp']), "%)", sep='')
    #
    #                 if results['img_with_shapes'] > 0:
    #                     print("item2s with not enough alignments: ",
    #                           results['shapes'] - results['alignment'], "/", results['shapes'],
    #                           " (", round(100 - 100 * results['alignment'] / results['shapes']), "%)", sep='')
    #
    #                     if results['alignment'] > 0:
    #                         print("item2s with not enough clean alignments: ", results['alignment'] -
    #                               results['clean_alignment'], "/", results['alignment'], " (",
    #                               round(100 - 100 * results['clean_alignment'] / results['alignment']), "%)", sep='')
    #
    #                         if results['clean_alignment'] > 0:
    #                             print("item2s with not enough realistic alignments: ", results['clean_alignment'] -
    #                                   results['realistic_alignment'], "/", results['clean_alignment'], " (",
    #                                   round(100 - 100 * results['realistic_alignment'] / results['clean_alignment']),
    #                                   "%)\n", sep='')
    #
    #                             print("Results per item1:")
    #                             for iitem1 in range(len(results['item1_measure'])):
    #                                 print("item1 ", iitem1 + 1, ": ", results['item1_measure'][iitem1],
    #                                       " (", round(100 * results['item1_measure'][iitem1] / results['img']), "%)",
    #                                       sep='')

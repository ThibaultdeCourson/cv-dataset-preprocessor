# Base packages
import os
from math import ceil, sqrt
from statistics import stdev
from os.path import isfile

import cv2 as cv
import numpy as np

# Project packages
from common import sort_list, display, to_bgr, bbx_retrieve, resize_shape, reframe, enlarge, \
    initialize_dir, shift_coords, get_point, prepare_output


def detect_crop_and_detect(param, img_list, images=None, orientation=None, visuals=None, name='item1', #, results=None
                           ):
    """
    FIRST DETECTION
    """

    detect_with_CNN(source=param['data_path'] + '/input/' + param['source'],
                    folder=param['item_name']['folder'],
                    param=param['labels'][name],
                    name=name)

    write = initialize_dir(path=param['data_path'] + '/input/' + param['source'] + '/' + param['item_name']['folder'] +
                                '_' + name + '/',
                           overwrite=False, create=True, warning=True)

    """
    SECOND DETECTION
    """

    print("images processed: ", end='')

    for filename in sorted(img_list, key=len):
        filename = str(filename)
        frame_number = os.path.splitext(filename)[0]

        print(frame_number, ", ", sep='', end='')

        # Load the image
        img = cv.imread(param['data_path'] + '/input/' + param['source'] + '/' + param['item_name']['folder'] + '/' + filename)
        # results['img'] += 1  # Count the image read
        if param['verbose'] > 3: display(img)  # Vanilla image
        # Visuals, for debug purpose
        if param['output']['img']['activate'] or param['verbose'] > 1: visuals[0] = img.copy()
        if param['output']['img']['activate'] or param['output']['video']['activate']: visuals[2] = img.copy()

        """
        # Detection zone
        detection_zone = {'left': param['detection_margin']['left'],
                          'right': img.shape[1] - param['detection_margin']['right'],
                          'top': param['detection_margin']['top'],
                          'bottom': img.shape[0] - param['detection_margin']['bottom']}
        if visuals[0] is not None: cv.rectangle(visuals[0], (detection_zone['left'], detection_zone['top']),
                                                (detection_zone['right'], detection_zone['bottom']), to_bgr('red'), 2)
        """

        # Recovery of bBoxes
        item1 = bbx_retrieve(param['data_path'] + '/input/' + param['source'] + '/' +
                             param['labels']['item1s']['name'] + '/labels/' + os.path.splitext(filename)[0] + '.txt',
                             img.shape)

        if len(item1) > 0:
            # orientation
            img_orientation = None
            if orientation is not None:
                # if param['suffix'] == '':
                if frame_number.isnumeric(): frame_number = int(frame_number)
                if frame_number in orientation:
                    img_orientation = orientation[frame_number]
                else:
                    print('no orientation value found')
                # else: img_orientation = orientation[param['suffix'].split('_')[1] + frame_number]

            A_median_item1s = np.median(np.array([item['A'] for item in item1]))
            r_median_item1s = np.median(np.array([item['r'] for item in item1]))
            analysis_zone = {}
            images.append({'filename': filename, 'number': frame_number, 'shape': img.shape, 'orientation': img_orientation,
                           'item1s': []})
            if img_orientation is not None:
                item1 = sort_list(item1, img_orientation)
            n_item1s = 0

            while item1:
                item1 = item1.pop(0)

                if is_compliant(item1, A_median_item1s, r_median_item1s, n_item1s, visual=visuals[0],
                                bBox_type='b'):
                    item1_bBox = {'left': item1['left'], 'right': item1['right'],
                                    'top': item1['top'], 'bottom': item1['bottom']}
                    if visuals[0] is not None:
                        # Display the item1 number
                        cv.putText(visuals[0], 'C' + str(n_item1s + 1), (item1['left'] - 30, item1['top'] - 5),
                                   cv.FONT_HERSHEY_COMPLEX_SMALL, 1, to_bgr('limegreen'), 1, cv.LINE_AA)
                    item1['item1'] = {'left': item1['left'], 'right': item1['right'],
                                          'top': item1['top'], 'bottom': item1['bottom']}
                    item1['target_point'] = False
                    item1['item2s'] = []

                    reframe(resize_shape(item1_bBox, {'top': -20, 'bottom': 15, 'left': 15, 'right': 15}), img.shape)
                    item1['shifting'] = (item1_bBox['left'], item1_bBox['top'])

                    item1['img'] = img[item1_bBox['top']: item1_bBox['bottom'],
                                         item1_bBox['left']: item1_bBox['right']]

                    if write: cv.imwrite(param['data_path'] + '/input/' + param['source'] + '/' + param['item_name']['folder'] + '_item1s/' +
                                         str(frame_number) + "_" + str(n_item1s) + ".jpg", item1['img'])

                    enlarge(analysis_zone, item1)  # Enlarge tbe analysis zone to wrap the item1

                    images[-1]['item1s'].append(item1)
                    n_item1s += 1

            # results['item1s'] += n_item1s  # Count the item1s identified by CNN

            # Expand the analysis zone around the item1s according to the number of item1s
            analysis_zone = resize_shape(analysis_zone, (param['analysis_padding']['multiple']
                                                         if param['n_item1s'] > 1 else
                                                         param['analysis_padding']['one']))
            # Use the orientation information to adjust the analysis zone
            if img_orientation is not None:
                if img_orientation in param['analysis_padding']:
                    resize_shape(analysis_zone, param['analysis_padding'][img_orientation])
            reframe(analysis_zone, img.shape)

            # Draw the analysis zone
            if visuals[0] is not None:
                cv.rectangle(visuals[0], (analysis_zone['left'], analysis_zone['top']),
                             (analysis_zone['right'], analysis_zone['bottom']), to_bgr('yellow'), 2)

            images[-1]['analysis_zone'] = analysis_zone
            images[-1]['shifting'] = (analysis_zone['left'], analysis_zone['top'])
            # Crop the image around the aalysis zone
            images[-1]['img'] = img[analysis_zone['top']:analysis_zone['bottom'],
                                    analysis_zone['left']:analysis_zone['right']]
        else:
            print(", no item1s detected", sep='', end='')

        # if bbx_recovery(item1s,
        #                 img,
        #                 ,
        #                 ,
        #                 param['n_item1s'],
        #                 detection_zone, analysis_zone, results, visuals[0],
        #                 param['verbose'] > 3, 'left' if img_orientation is None else img_orientation)

        # content = '_'
        # if 'item1' in item1s[iitem1]:
        # elif item1s[iitem1]['item2s'][1]['position'] is None:
        #     content += 'o'
        #     a = resize_shape(item1s[iitem1], {'top': 50, 'bottom': 50, 'left': 150, 'right': 125})
        # else:
        #     content += 't'
        #     a = resize_shape(item1s[iitem1], {'top': 15, 'bottom': 15, 'left': 15, 'right': 15})

        # a = reframe(a, img.shape)
        # cropped_img = img[a['top']: a['bottom'], a['left']: a['right']]
        # #if content != '_a':
        # cv.imwrite(param['output']['img']['name'] + '/data/' + frame_number + "_" + str(iitem1) + content + ".jpg",
        #            cropped_img)
        #if param['verbose'] > 2: display(visuals[0])
    print("\nCropped images created")
    detect_with_CNN(param['source'], param['item_name']['folder'] + '_item1s', param['labels']['item2'], 2, 'item2')


def is_compliant(bBox, A_median, r_median, n_obj=3, tolerance=0.6, visual=None, bBox_type=None):
    if bBox_type is None:
        bBox_type = "item2"
        colors = ('lime', 'tomato')
    else:
        bBox_type = "item1"
        colors = ('limegreen', 'red')

    if (1-tolerance < bBox['A'] / A_median < 1 + tolerance and 1-tolerance < bBox['r'] / r_median < 1 + tolerance) or\
            (n_obj < 3 and not bBox['cropped']):
        if visual is not None:
            cv.rectangle(visual, (bBox['left'], bBox['top']),
                         (bBox['right'], bBox['bottom']), to_bgr(colors[0]), 2 if bBox_type == "item2" else 1)
        return True
    else:
        #print("Removing", round(bBox['A'] / A_median, 2), round(bBox['r'] / r_median, 2), bBox)
        print("parasite", bBox_type, "removed, ", end='')
        if visual is not None:
            cv.rectangle(visual, (bBox['left'], bBox['top']),
                         (bBox['right'], bBox['bottom']), to_bgr(colors[1]), 2)
        #bBoxes.pop(0)
        #display(visual)
        return False


def target_point_detection(img, bBoxes):
    """
        return a list of target points
        :param im:
        :param bBoxes: bBoxe de la forme {'x': 'y': 'left': 'right': 'top': 'bottom': 'w': 'h':}
        :return: list of target points
        bBoxe normalise selon la taille de l'image
        """

    target_points = []
    for i, bBox in enumerate(bBoxes):
        x, y, h, w = bBox['yc'], bBox['xc'], round(bBox['h'] / 2), round(bBox['w'] / 2)

        target_point = None
        mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)

        cv.rectangle(mask, (bBox['left'], bBox['bottom']), (bBox['right'], bBox['top']), 255, -1)
        img1 = cv.bitwise_and(img.copy(), img.copy(), mask=mask)  # Cropping the bBox

        #TODO: target_point =

        target_points.append(target_point)

    return target_points


def item1s_recovery(image, item1_label, n_item1s, visuals, results, verbose):
    n_item1s = len(image['item1s'])
    if n_item1s != n_item1s: print("detected: ", n_item1s, " item1s ", sep='', end='')

    total_item1s = 0
    n_target_points = 0

    for i, item1 in enumerate(image['item1s']):
        # Recovery of item1 bBoxes
        item1s = bbx_retrieve(item1_label + str(i) + '.txt', item1['img'].shape)  # legacy: detection_zone

        if len(item1s) > 0:

            item1['shifting'] = shift_coords(item1['shifting'], image['shifting'], inverse=True)
            item1s = sort_list(item1s, image['orientation'])
            n_item1s = 0
            while item1s:
                item1 = item1s.pop(0)

                # Enlarge the item1 bBox to wrap the item1 bBox (bugged but would be pertinent to debug it)
                #enlarge(item1, item1, shifting=True)

                if visuals[1] is not None:
                    cv.rectangle(visuals[1],
                                 shift_coords((item1['left'], item1['top']), item1['shifting']),
                                 shift_coords((item1['right'], item1['bottom']),
                                              item1['shifting']),
                                 to_bgr('lime'), 2)
                    cv.rectangle(visuals[0],
                                 shift_coords((item1['left'], item1['top']), item1['shifting']),
                                 shift_coords((item1['right'], item1['bottom']),
                                              item1['shifting']),
                                 to_bgr('lime'), 2)
                    #display(visuals[0])

                item1['position'] = i + n_item1s
                cv.putText(visuals[1], 'W' + str(item1['position'] + 1),
                           shift_coords((item1['left'] - 30, item1['bottom'] + 15),
                                        item1['shifting']),
                           cv.FONT_HERSHEY_COMPLEX_SMALL, 1, to_bgr('lime'), 1, cv.LINE_AA)
                n_item1s += 1
                total_item1s += 1

                item1['item1s'].append(item1)

            bBoxes = list(filter(lambda item2: item2['position'] is not None, item1['item2s']))

            if len(bBoxes) > 0:
                target_points = detection_tools.target_point_detection(item1['img'], bBoxes)
            n_target_points += len(target_points)

    if total_item1s > 0:
        results['item1s'] += total_item1s  # Count the item1s identified by CNN

        if n_target_points > 0:
            if n_target_points < total_item1s:
                print(n_target_points, " target point", ('' if n_target_points == 1 else 's'), " found, ", sep='',
                      end='')
            if verbose: display(visuals[1])
            results['img_with_tp'] += 1  # Count the images with at least one target point identified
            return True
        else:
            print("no target point found", end='')
    else:
        print("no item1 detected", end='')

    return False


def basis_change(bBox, translation_bBox):
    bBox['left'] = translation_bBox[0] + bBox['left']
    bBox['right'] = translation_bBox[0] + bBox['right']
    bBox['top'] = translation_bBox[1] + bBox['top']
    bBox['bottom'] = translation_bBox[1] + bBox['bottom']
    bBox['xc'] = translation_bBox[0] + bBox['xc']  # coordonnée centre bounding box reporter sur l'image initiale
    bBox['yc'] = translation_bBox[1] + bBox['yc']  # coordonnée centre bounding box reporter sur l'image initiale


def get_output(target_point, shapes, shape, visual, results, verbose):
    # shape1s, imL = compute_shape1(img, target_point)
    # shape1s, imL = compute_shape1(img, target_point, tmp)

    outputs = []
    theta = 0

    # Compute the ratio on a shape1 with a specific theta, while we do not have at least 5 ratios we start again with
    # a new shape1
    while theta <= 180:
        shape1 = get_point(target_point[0], target_point[1], theta, shape[1], shape[0]), \
               get_point(target_point[0], target_point[1], theta + 180, shape[1], shape[0])
        # mask of the shape1

        shape1_mask = np.zeros((shape[0], shape[1]), dtype=np.uint8)
        cv.line(shape1_mask, shape1[0], shape1[1], 255, 1)

        #display(shape1_mask | shapes['shape1']['mask'] | shapes['shape3']['mask'] | shapes['shape2']['mask'], "Masks")
        intersections = {}
        for marking in shapes:
            intersections[marking] = find_closest_intersection(target_point, shapes[marking]['mask'], shape1_mask)

        combinations = []
        if intersections['shape2'] is not None:
            if intersections['shape1'] is not None:
                combinations += [('shape2', 'shape1')]
            elif intersections['shape3'] is not None:
                combinations += [('shape2', 'shape3')]
        if intersections['shape1'] is not None and intersections['shape3'] is not None:
            combinations += [('shape1', 'shape3')]

        for combination in combinations:
            shape1s_output = get_output(intersections[combination[0]], intersections[combination[1]])
            if shape1s_output > 5:
                dot = np.dot(target_point - intersections[combination[0]],
                             intersections[combination[1]] - intersections[combination[0]])

                item2_output = get_output(target_point, intersections[combination[0]]) * dot / abs(dot)
                real_output = (item2_output *
                                 (shapes[combination[1]]['parameter'] - shapes[combination[0]]['parameter'])) / \
                                shape1s_output + shapes[combination[0]]['parameter']
                # print('-'.join(combination), real_output)
                """
                v = visual.copy()
                cv.line(v, intersections[combination[0]], intersections[combination[1]],
                        (255, 255, 0), 1)
                cv.circle(v, intersections[combination[0]], 4, to_bgr('white'), -1)
                cv.circle(v, intersections[combination[1]], 4, to_bgr('black'), -1)
                cv.circle(v, target_point, 4, to_bgr('yellow'), -1)
                display(v)
                print(round(real_output), "cm")
                """
                if -20 < real_output < 465:
                    # print("CORRECT")
                    if visual is not None:
                        cv.line(visual, intersections[combination[0]], intersections[combination[1]],
                                (255, 255, 0), 1)
                        cv.circle(visual, intersections[combination[0]], 2, (255, 255, 255), -1)
                        cv.circle(visual, intersections[combination[1]], 2, (0, 165, 255), -1)

                    outputs.append(real_output)
                # else: print("WRONG")
                # display(v, "here")

        theta += 3

    output = None
    # if verbose > 2: display(visual)
    if len(outputs) > 1:

        # Re-draws the target points on the image
        if visual is not None: cv.circle(visual, target_point, 3, (0, 255, 255), -1)

        results['alignment'] += 1
        # find absolute value of z-score for each observation
        z = np.abs(zscore(outputs))

        # only keep rows in dataframe with all z-scores less than absolute value of 3
        parasites = [round(outputs[i]) for i in range(len(outputs)) if z[i] >= 1.5]
        if verbose > 1 and len(parasites) > 0:
            print("False values", parasites, "eliminated, ", end='')
        outputs = [outputs[i] for i in range(len(outputs)) if z[i] < 1.5]

        if len(outputs) > 2:
            results['clean_alignment'] += 1
            st = stdev(outputs)  # ecart type
            if st < 10:
                results['realistic_alignment'] += 1
                output = float(np.mean(outputs))
                if verbose > 1:
                    print("output: ", round(output), "cm (mean on ", len(outputs), " values)", sep='')
                if visual is not None: cv.putText(visual, str(round(output)) + " cm",
                                                  (target_point[0] + 5, target_point[1] + 5),
                                                  cv.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255), 1, cv.LINE_AA)
            elif verbose > 1:
                print("output measures too scattered")
        elif verbose > 1:
            print("Not enough correct alignments found")
    elif verbose > 1:
        print("Not enough alignments found")
    return output


def get_deviation_from_shape1s(results, img, target_points, track_det, visual, masks, shape, shifting=(0, 0), verbose=0):
    outputs = [None] * CNN['n_item1s'] * 2

    for i in range(len(target_points)):
        if verbose > 1: print("\nitem2 ", (i + 1), ": ", sep='', end='')
        target_point = target_points[i]
        outputs = []
        theta = 0

        # Compute the ratio on a shape1 with a specific theta, while we do not have at least 5 ratios we start again with
        # a new shape1
        while theta < 180:

            print("\n\npoint", target_point, ", Angle", theta, "° =", round(theta * pi / 180, 2),
                  "rad, Size", img.shape[1], img.shape[0])

            A, B = get_point(target_point[0], target_point[1], theta, img.shape[1], img.shape[0]), \
                   get_point(target_point[0], target_point[1], theta + 180, img.shape[1], img.shape[0])
            if theta > 90: A, B = B, A

            # mask of the line
            line_mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)

            cv.line(line_mask, (A[0] - 1, A[1] - 1), (B[0] - 1, B[1] - 1), 255, 1)

            length = ceil(sqrt((A[0] - B[0]) ** 2 + (A[1] - B[1]) ** 2)) - 100
            line_coords = np.vstack((np.linspace(A[1], B[1], length), np.linspace(A[0], B[0], length)))

            if False:
                line = np.zeros((1, length, 3), dtype='uint8')
                for channel in range(3):
                    line_channel = map_coordinates(img[:, :, channel], line_coords)
                    line[:, :, channel] = line_channel
                    print("BGR channel ", channel + 1, ";", ";".join(map(str, line_channel)), sep='')
                for name, conversion in {'HSV': (cv.COLOR_BGR2HSV, (255 / 179, 1, 1)),
                                         'LAB': (cv.COLOR_BGR2LAB, (1, 1, 1)),
                                         'YCrCb': (cv.COLOR_BGR2YCrCb, (1, 1, 1))}.items():
                    color = cv.cvtColor(img, conversion[0])
                    for channel in range(3):
                        print(name, " channel ", channel + 1, ";",
                              ";".join(map(str, map_coordinates(color[:, :, channel], line_coords))), sep='')

            theta += 5
    return outputs, results


def detect_with_CNN(source, folder, param, name=''):

    if prepare_output(path=source + '/' + param['name'], overwrite=param['overwrite'], output_type='CNN'):
        # If CNN is activated

        #print("\nWARNING: No " + name + " labels found, CNN activated\n")

        # Load the YAML file of the training used for detection
        if isfile(param['weights_path'] + '/opt.yaml'):
            import yaml

            with open(param['weights_path'] + '/opt.yaml') as fh:
                training_data = yaml.load(fh, Loader=yaml.FullLoader)  # Load YAML data from the file

            imgsz = [training_data['imgsz']]
            if type(imgsz) == list:
                if len(imgsz) == 1:
                    imgsz = [ceil(imgsz[0] / 32) * 32, ceil(imgsz[0] / 32) * 32]
                else:
                    imgsz = [ceil(i / 32) * 32 for i in reversed(imgsz)]
            else:
                imgsz = [ceil(imgsz / 32) * 32, ceil(imgsz / 32) * 32]
        else:
            imgsz = 640


        # Executes the CNN
        if param['CNN'] == 'yolov8':
            from ultralytics import YOLO

            # Load a model
            if param['weights_path'] is None:
                model = YOLO('yolov8n-cls.pt')  # load a pretrained model (recommended for training)
            else:
                model = YOLO(param['weights_path'] + '/best.pt')

            model.predict(source=source + '/' + folder, save=True, save_txt=True, save_conf=True, show_labels=True,
                          show_boxes=True)  # , save_crop=True, show_conf=True

        elif param['CNN'] == 'yolov5':
            # yolov5 must be locally imported
            from yolov5 import detect

            detect.run(source=source + '/' + folder,
                       weights=param['weights_path'] + '/weights/' + ('best' if param['best_weights'] else 'last') + '.pt',
                       save_txt=True,
                       project=source,
                       name=param['name'],
                       imgsz=imgsz,
                       nosave=not param['save_img'],
                       max_det=param['n'],
                       conf_thres=0.25,
                       save_conf=True)
    else:
        print(name + " detection using the labels from the last CNN run\n")


def train_CNN(source_path, param, output_path='.', n_objects=2, training_name="train"):

    if isfile(source_path + 'runs/train/weights/best.pt'): #prepare_output(path=source + '/' + param['name'], overwrite=param['overwrite'], output_type='CNN'):
        print("Using the labels from the last CNN run\n")
        return None

    else:
        # If CNN is activated

        # Executes the CNN
        if param['CNN'] == 'yolov8':
            print('\n--- USING YOLOV8 ---')

            from ultralytics import YOLO

            # Load a model
            model = YOLO('yolov8n-cls.pt')  # load a pretrained model (recommended for training)

            # Train the model
            results = model.train(data=source_path,
                                  epochs=100,  # time= (heures)
                                  batch=16,
                                  patience=10,
                                  freeze=9,
                                  imgsz=685,
                                  save=True, save_period=5, resume=False,
                                  project=output_path + '/runs',
                                  exist_ok=False,
                                  name=training_name,
                                  # seed=42,
                                  verbose=True
                                  )
            return model.metrics

        elif param['CNN'] == 'yolov5':
            print('\n--- USING YOLOV5 ---')
            # yolov5 must be locally imported
            return None  # TODO
# Base packages
from random import randint
import cv2 as cv
import numpy as np

# item2 detection functions
from common import filter_colors, to_opencv_hsv, simplify_angle, octet_rectifier, to_bgr, display

range_colors = {'HSV': {'min': [[256, -1], [256, -1], [256, -1]], 'max': [[-1, -1], [-1, -1], [-1, -1]]},
                'BGR': {'min': [[101, -1], [101, -1], [101, -1]], 'max': [[-1, -1], [-1, -1], [-1, -1]]}}


def auto_adjusting_filters(img, shapes, source, name, frame_number=0, only_analysis=True, file=None):
    average_color = {'BGR': img.mean(axis=0).mean(axis=0),
                     'cvHSV': cv.cvtColor(img, cv.COLOR_BGR2HSV).mean(axis=0).mean(axis=0)}
    average_color['HSV'] = to_opencv_hsv(average_color['cvHSV'], inv=True)
    BxGxR = np.prod(average_color['BGR'])
    intensity = np.sum(average_color['BGR'])
    percBxGxR = BxGxR / intensity ** 3
    average_color['%BGR'] = [color / intensity for color in average_color['BGR']]

    if not only_analysis:
        for i in range(3):
            for code in ('HSV', 'BGR'):
                if average_color[code][i] > range_colors[code]['max'][i][0]:
                    range_colors[code]['max'][i] = [average_color[code][i], frame_number]
                elif average_color[code][i] < range_colors[code]['min'][i][0]:
                    range_colors[code]['min'][i] = [average_color[code][i], frame_number]

    # print('\ninput:\ncvHSV:', average_color['cvHSV'], '%BGR:', average_color['BGR'][0]/intensity)
    print("AUTO FILTER ACTIVATED")
    shapes['shape1']['lower'] = (-19260 + 524407.6131 * percBxGxR,  ##shapes['shape1']['lower'][0],
                                     155 - 0.01653 * average_color['cvHSV'][0] * average_color['cvHSV'][1],
                                     -29 + 1.11577 * average_color['BGR'][0])
    shapes['shape1']['upper'] = (-21762 + 593518.99227 * percBxGxR,
                                     275 - 0.00025 * np.prod(average_color['cvHSV']),
                                     138 + 2e-05 * BxGxR)

    # print(average_color['%BGR'][0], average_color['cvHSV'][0]*average_color['cvHSV'][1]*average_color['cvHSV'][2])
    for i in range(3):
        if shapes['shape1']['lower'][i] >= shapes['shape1']['upper'][i]:
            print("ERROR on", i, ":", shapes['shape1']['lower'][i], ">", shapes['shape1']['upper'][i])
        else:
            print("RIGHT:", shapes['shape1']['lower'][i], "<", shapes['shape1']['upper'][i])

    # print("Raw filter range:\n", list(map(round, shapes['shape1']['lower'])), "\n", list(map(round, shapes['shape1']['upper'])))

    # print(shapes['shape1']['lower'][1], shapes['shape1']['upper'][1])
    print(shapes['shape1']['lower'][0], ";", shapes['shape1']['upper'][0], ";",
          shapes['shape1']['lower'][1], ";", shapes['shape1']['upper'][1], ";",
          shapes['red_line']['lower'][2], ";", shapes['red_line']['upper'][2], sep='')
    # print(shapes['red_line']['lower'][0]/179*360, ";", shapes['red_line']['upper'][0]/179*360, ";",
    #       shapes['red_line']['lower'][1]/255*100, ";", shapes['red_line']['upper'][1]/255*100, ";",
    #       shapes['red_line']['lower'][2]/255*100, ";", shapes['red_line']['upper'][2]/255*100, sep='')
    print("Raw filter range:\n",
          shapes['red_line']['lower'][0] / 179 * 360, "->", shapes['red_line']['upper'][0] / 179 * 360, "\n",
          shapes['red_line']['lower'][1] / 255 * 100, "->", shapes['red_line']['upper'][1] / 255 * 100, "\n",
          shapes['red_line']['lower'][2] / 255 * 100, "->", shapes['red_line']['upper'][2] / 255 * 100, "\n")

    for marking in shapes:
        for bound in ('lower', 'upper'):
            shapes[marking][bound] = (simplify_angle(shapes[marking][bound][0]),
                                        octet_rectifier(shapes[marking][bound][1]),
                                        octet_rectifier(shapes[marking][bound][2]))

    print("Corrected filter", shapes['red_line']['lower'], shapes['red_line']['upper'])

    if not only_analysis:

        if file is not None:
            file.write(";".join(map(str, average_color['cvHSV'].tolist() + average_color['BGR'].tolist())) + ';')
        cropped_visual = img.copy()

        cv.rectangle(cropped_visual, (10, 10), (50, 50), average_color['BGR'], -1)

        textes = [["Hue:", (60, 20)], [round(average_color['HSV'][0]), (105, 20)],
                  ["Sat:", (60, 35)], [str(round(average_color['HSV'][1])) + "%", (105, 35)],
                  ["Val:", (60, 50)], [str(round(average_color['HSV'][2])) + "%", (105, 50)],
                  ["Blue:", (155, 20)], [round(average_color['BGR'][0]), (220, 20)],
                  ["Green:", (155, 35)], [round(average_color['BGR'][1]), (220, 35)],
                  ["Red:", (155, 50)], [round(average_color['BGR'][2]), (220, 50)],
                  ["(" + str(round(100 * average_color['%BGR'][0], 1)) + "%)", (255, 20)],
                  ["(" + str(round(100 * average_color['%BGR'][1], 1)) + "%)", (255, 35)],
                  ["(" + str(round(100 * average_color['%BGR'][2], 1)) + "%)", (255, 50)],
                  [frame_number, (5, round(cropped_visual.shape[0] * 0.98))]
                  ]

        for text in textes:
            cv.putText(cropped_visual, str(text[0]), text[1], cv.FONT_HERSHEY_COMPLEX_SMALL, 0.75, (255, 255, 255), 1,
                       cv.LINE_AA)
        cv.imwrite(param['data_path'] + '/output/' + source + '/frames_cropped/' + name, cropped_visual)


def shapes_detection(img, shapes, analysis_zone, exclude, visual, results):
    shape2s = []
    if visual is not None:
        for bBox in exclude:
            # Draws item2s bounding boxes on the visual
            cv.rectangle(visual, bBox[0], bBox[1], to_bgr('limegreen'), 1)
    for marking in shapes:
        if shapes[marking]['import_mask']:
            shapes[marking]['mask'] = cv.Canny(shapes[marking]['mask'][analysis_zone['top']:analysis_zone['bottom'],
                                                 analysis_zone['left']:analysis_zone['right']], 400, 400 * 2)
            shape2s += [marking]

        else:
            if marking == 'shape2':
                shapes[marking]['mask'] = find_band(img, shapes[marking]['lower'], shapes[marking]['upper'])
            else:
                shapes[marking]['mask'] = find_line(img, shapes[marking]['lower'], shapes[marking]['upper'],
                                                      exclude)

            if np.any(shapes[marking]['mask']):
                shape2, _ = cv.findshape2s(shapes[marking]['mask'], cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
                shape2s += [marking]
                # display(cv.drawlines(img.copy(), shape2, -1, (255, 255, 255), 1))
                if visual is not None: cv.drawlines(visual, shape2, -1, shapes[marking]['color'], 1)
                results[marking] += 1

    return shape2s


def content_searcher(data, source=True):
    if source:
        content = "\nContent of the variable:\n"
    else:
        content = "\n"
    if isinstance(data, np.ndarray) or type(data) is list or type(data) is tuple or type(data) is dict:
        return content + str(type(data)) + " of length " + str(len(data)) + content_searcher(
            data[randint(0, len(data) - 1)], source=False)
    else:
        return content + str(type(data)) + "\n"


def find_line(img, lower_color, upper_color, exclude=None):
    # mask_crop = cv.bitwise_and(img_filter, img_filter, mask=crop_backgroung(img, 6))  # crops the background

    # print("\nRed filters: ", lower_color, upper_color)

    # Filter the red zones
    img_filter = filter_colors(img, lower_color, upper_color)  # , verbose=True)
    if exclude is not None:
        for bBox in exclude:
            cv.rectangle(img_filter, bBox[0], bBox[1], 0, thickness=-1)

    """
    temp = cv.bitwise_and(img, img, mask=img_filter)
    temp[img_filter == 0] = 255
    display(temp)
    temp = cv.bitwise_and(img, img, mask=cv.bitwise_not(img_filter))
    temp[img_filter == 255] = 255
    display(temp)
    """

    # Smooth the shape2s
    img_red_filtered = cv.morphologyEx(img_filter, cv.MORPH_OPEN,
                                       cv.getStructuringElement(cv.MORPH_ELLIPSE, (3, 3)))

    # display(cv.bitwise_and(img, img, mask=img_red_filtered))

    t = 400
    LR_mask = cv.Canny(img_red_filtered, t, t * 2)  # Filter the edges

    # mask_red_f = cv.blur(img_red_filtered, (3, 3))

    return LR_mask


def find_band(img, lower_color, upper_color):
    CA_mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    CA_shape2s = []
    # Filter the color
    img_filter = filter_colors(img, lower_color, upper_color)

    # display(cv.bitwise_and(img, img, mask=img_filter))
    # display(cv.bitwise_and(img, img, mask=cv.bitwise_not(img_filter)))

    img_dilated = cv.dilate(img_filter, cv.getStructuringElement(cv.MORPH_ELLIPSE, (4, 4)),  # (8, 8)
                            iterations=1)  # removes impurities

    shape2s, _ = cv.findshape2s(img_dilated, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

    # Generates the list of the size of the shape2s with the id of the associated shape2
    shape2s_size = [(cv.shape2Area(shape2s[i]), i) for i in range(len(shape2s))]

    if len(shape2s_size) >= 1:
        CA_shape2s_ids = []
        shape2s_size = sorted(shape2s_size, reverse=True)

        max_Area = shape2s_size[0][0]  # shape2 with the maximum air
        if max_Area <= 70000:
            # if the shape2 with the maximum air is not big enough it means that we do not detect the whole CA so
            # we take the first 3 biggest shape2s
            for j in range(3):
                if j < len(shape2s_size):
                    if shape2s_size[j][0] >= 11000:
                        CA_shape2s_ids.append(shape2s_size[j][1])
        else:
            CA_shape2s_ids.append(shape2s_size[0][1])

        for shape2_id in range(len(shape2s)):
            if shape2_id in CA_shape2s_ids:
                # cv.drawlines(visual, shape2s, shape2_id, (0, 255, 0), 1)
                CA_shape2s.append(shape2s[shape2_id])
            # else: cv.drawlines(visual, shape2s, shape2_id, (0, 0, 255), 1)

    cv.drawlines(CA_mask, CA_shape2s, -1, 255, 1)

    return CA_mask

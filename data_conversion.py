import os
import pandas as pd
from tqdm import tqdm
import cv2 as cv
import numpy as np
from sklearn.model_selection import KFold
from random import sample
from common import initialize_dir

video_formats = ('mp4', 'mkv', 'mov', 'm2ts')


class Dataset:

    def __init__(self, source_path, task, cropping, params, training=False, source_type=None, item_name=None,
                 video_cut=None, fps=None):

        overwrite = params['overwrite']
        self.cropping = cropping
        self.images = {}
        #self.suffix = ''

        if training:
            source_type = params['source_type']
            item_name = params['item_name']
            #source = params['source']
            source_path += params['source']
            self.splitting_config = params['splitting_config']
            if task == 'classification':
                self.classes = {}
        # elif source_path is None:
        #     raise ValueError("Error: a source_path value must be specified for non-training datasets")

        print("\nInput data:", source_path + '/', end='')
        if item_name is None:
            item_name = {'folder': 'frames', 'img': 'image', 'video': 'video'}
        #suffix = ''

        if source_type is None:
            """
            FOLDER OF IMAGES
            """

            print(item_name['folder'])

            # Gets the list of files to study
            if os.path.isdir(source_path + '/' + item_name['folder']):
                self.images = None  # TODO: import images using
                # sorted(os.listdir(source_path + '/' + item_name['folder']), key=len)
            else:
                raise ValueError("Error: the directory " + source_path + "/" + item_name['folder'] + " does not exists")
        else:
            # If the input is a file
            if source_type.lower() in ('bmp', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'dng', 'webp', 'mpo'):
                """
                SINGLE IMAGE
                """

                print(item_name['img'] + '.' + source_type)

                # If the input is a single image
                if os.path.isfile(source_path + '/' + item_name['folder'] + '/' +
                                  item_name['img'] + '.' + source_type):
                    self.images[
                        item_name['img']] = None  # TODO: import image using item_name['img'] + '.' + source_type
                else:
                    raise ValueError("Error: the image " + source_path + '/' + item_name['folder'] + '/'
                                     + item_name['img'] + '.' + source_type + " does not exists")

            elif source_type.lower() in video_formats:
                """
                VIDEO
                """

                #print(item_name['video'] + '.' + source_type)
                # if not (task == 'classification' and training):
                #     if start != 0 or end is not None:
                #         suffix += "_(" + str(start) + "," + (-1 if end is None else str(end)) + ")"
                # if self.fps > 0:
                #     suffix += "_" + str(self.fps) + "fps"
                #
                # self.suffix = suffix

                generated_folder = source_path + '/' + item_name['folder']  #+ suffix
                if initialize_dir(generated_folder, overwrite=overwrite):

                    if training:
                        self.start, self.end = params['video_cut']
                        self.fps = params['fps']

                        if task == 'classification':

                            self.process_classification_videos(input_path=source_path + '/',
                                                               output_path=generated_folder,
                                                               balancing=params['balancing'])

                        elif task == 'detection':
                            pass  # TODO
                    else:
                        self.start, self.end = video_cut
                        self.fps = fps

                        video['video'].convert_to_images(
                            file=source_path + '/' + item_name['video'] + '.' + source_type,
                            verbose=True)
                        self.write_images(path=generated_folder)
                else:
                    print("Video already converted to frames with the current parameters\n")

            else:
                raise ValueError("Error: " + source_type + " format not supported")

        # TODO: Perform data augmentation on the input data
        # if augment:
        #     from data_augmentation import data_augment
        #     data_augment(path, path, param['data_path'] + '/input', 5)

    def process_classification_videos(self, input_path, output_path, balancing):

        videos = []
        for item in os.listdir(input_path):
            if os.path.isfile(input_path + item):
                this_video = Video(path=input_path, filename=item)
                #video, fps_video, n_frames = self.get_video(file=input_path + item)

                end = this_video.n_frames / this_video.fps_video if self.end is None else self.end
                videos.append({'video': this_video, 'end': end, 'n_selec_frames': round((end - self.start) * self.fps)})

        max_size = None
        if balancing:
            max_size = min([video['n_selec_frames'] for video in videos])

        for video in videos:
            video['video'].convert_to_images(start=self.start, end=video['end'], chopping_fps = self.fps,
                                             cropping=self.cropping,
                                             n_selec_frames=video['n_selec_frames'],
                                             max_size=max_size, verbose=True)
            video['video'].write_images(path=output_path + '/', subfolder=True, splitting_config=self.splitting_config)

    def convert_images_to_video(self, source_path='', output_path='', frame_size=(1920, 1080), name='video',
                                video_format='mp4', output_fps=10):
        if os.path.isfile(output_path + '/' + name + '.' + video_format):
            print("Warning: previous video deleted")
            os.remove(output_path + '/' + name + '.' + video_format)

        # choose codec according to format needed
        video = cv.VideoWriter(output_path + '/' + name + '.' + video_format, cv.VideoWriter_fourcc(*'mp4v'),
                               output_fps,
                               frame_size)

        print("Assembling video...")
        for filename in sorted(os.listdir(source_path), key=len):
            video.write(cv.imread(source_path + '/' + filename))

        cv.destroyAllWindows()
        video.release()
        print("Video assembled")


class Video:

    def __init__(self, path, filename):

        self.imgs = {}

        n_extension = filename.rfind(".")  # Locate the extension in the name
        if n_extension == -1:
            raise ValueError("Cannot find an extension in the name [" + str(filename) + "]")
        elif filename[n_extension + 1:].lower() not in video_formats:
            raise ValueError("[" + str(filename[n_extension + 1:].lower()) + "] is not a supported extension")
        else:
            self.name = filename[:n_extension]  # Remove the extension from the name
            # print("Processing class [", class_name, "]", sep='')

            self.video = cv.VideoCapture(path + filename)
            self.fps_video = self.video.get(cv.CAP_PROP_FPS)
            self.n_frames = self.video.get(cv.CAP_PROP_FRAME_COUNT)

    def convert_to_images(self, start, end, chopping_fps, cropping, n_selec_frames, max_size,
                          verbose=False):

        if chopping_fps > self.fps_video:
            raise ValueError("The frame rate chosen (" + str(chopping_fps) +
                             " is higher than the frame rate of the video (" + str(self.fps_video))
        if end is not None and end > self.n_frames / self.fps_video:
            raise ValueError("The video is shorter " + str(self.n_frames / self.fps_video) +
                             " than the end bound chosen rate chosen (" + str(end))

        if verbose:
            print("\nConversion of the video [", self.name, "] to frames: retreiving ", sep='', end='')
            if max_size is not None and max_size < n_selec_frames:
                print(max_size, "frames randomly selected from ", end='')
            print(n_selec_frames, "frames", end='')
            if start != 0:
                print(" from the ", start, "s", sep='', end='')
            if end is not None:
                print(" to the ", end, "s", sep='', end='')

            if chopping_fps != -1 and chopping_fps != self.fps_video:
                print(" (", chopping_fps, "fps)", sep='', end='')

        end_frame = self.n_frames if (end is None) else end * self.fps_video
        start_frame = start * self.fps_video if start < end_frame else 0
        range_video = np.linspace(start_frame, end_frame, n_selec_frames, dtype=int)
        #np.arange(start_frame, end_frame, self.n_frames)

        if max_size is not None and max_size < n_selec_frames:
            range_video = np.random.choice(range_video, size=max_size)

        for i in (tqdm(range_video) if verbose else range_video):
            # Set the position of the video capture object to the desired position
            self.video.set(cv.CAP_PROP_POS_FRAMES, int(i))

            _, image = self.video.retrieve()
            if cropping is not None:
                if (cropping['y_min'] is not None and cropping['y_max'] is not None
                        and cropping['x_min'] is not None and cropping['x_max']):
                    image = image[cropping['y_min']: cropping['y_max'],
                            cropping['x_min']: cropping['x_max']]
                else:
                    if cropping['y_max'] is None and cropping['y_min'] is not None:
                        image = image[cropping['y_min']:, :]
                    elif cropping['y_min'] is None and cropping['y_max'] is not None:
                        image = image[:cropping['y_max'], :]
                    if cropping['x_max'] is None and cropping['x_min'] is not None:
                        image = image[:, cropping['x_min']:]
                    elif cropping['x_min'] is None and cropping['x_max'] is not None:
                        image = image[:, :cropping['x_max']]

            name = str(round(i // self.fps_video)) + '-' + str(
                round(i % self.fps_video))  # str(round(i%self.fps_video/self.fps_video*chopping_fps))

            if name in self.imgs:
                raise ValueError("A frame named [" + name + "] already exists")
            else:
                self.imgs[name] = image

        self.video.release()

    def write_images(self, path, subfolder=False, splitting_config=False):
        self.imgs = pd.Series(self.imgs)

        if not splitting_config:
            if subfolder:
                path = path + self.name

            initialize_dir(path)

            for name, img in self.imgs:
                cv.imwrite(path + "/" + name + ".png", img)  # save frame as JPEG file

        elif len(splitting_config) == 3:
            pass

        elif len(splitting_config) == 1:
            kf = KFold(n_splits=splitting_config[0], shuffle=True, random_state=42)
            i_fold = 1

            #print("Generating 5-Fold datasets:")

            for fold_idxs in kf.split(self.imgs):
                fold_path = path + 'fold' + str(i_fold) + '/'
                for i in range(2):
                    dataset_path = fold_path + ("train" if i == 0 else "val") + '/' + self.name
                    initialize_dir(dataset_path, overwrite=False)
                    for name, img in self.imgs.iloc[fold_idxs[i]].items():
                        cv.imwrite(dataset_path + "/" + name + ".png", img)  # save frame as JPEG file
                i_fold += 1
            print("Class [", self.name, "]: ", len(fold_idxs[0]), " imgs for training, ", len(fold_idxs[1]),
                  " for validation", sep='')
        else:
            raise ValueError(
                "Error: Invalid format used for the argument 'splitting_config', length should be 2 or 3:" +
                str(splitting_config))

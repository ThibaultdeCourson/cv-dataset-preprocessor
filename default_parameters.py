# Parameters list
params = {
    # Source selection (only in the default parameters file)
    'data_path': '..',

    # Configuration
    'CNNs': {
        'state_detection': {
            'task': 'classification',
            'CNN': 'yolov8',
            'weights_path': 'input/yolov8l_100e',
            'n': None,
            'cropping': {'x_min': 770, 'x_max': 1170, 'y_min': 200, 'y_max': 885},
            'train': {
                'source_type': 'MP4',  # Type of item to recover (extension for a file, None for a folder)
                'item_name': {'folder': 'frames', 'img': 'image', 'video': 'video'},  # Name of the item to recover
                'video_cut': [0, None],  # Part of the video to study (in seconds)
                'fps': 0.5,  # Sampling frequency (for video file)

                # Train specific
                'source': 'state_detection',
                'augmentation': True,  # Perform data augmentation on input data
                'balancing': True,
                'overwrite': True,
                'splitting_config': (5,),  # Set to False to avoid splitting
            },
            'use': {
                'source_type': 'MP4',  # Type of item to recover (extension for a file, None for a folder)
                'item_name': {'folder': 'frames', 'img': 'image', 'video': 'video'},  # Name of the item to recover
                'video_cut': [0, None],  # Part of the video to study (in seconds)
                'fps': 0.1,  # Sampling frequency (for video file)
                'overwrite': False,
                'name': 'labels',
                'save_img': False,
                'best_weights': True
            }
        },
        'defect_detection': {
            'task': 'classification',
            'CNN': 'yolov8',
            'weights_path': 'input/yolov8l_100e',
            'n': None,
            'train': {
                'source_type': 'MP4',  # Type of item to recover (extension for a file, None for a folder)
                'item_name': {'folder': 'frames', 'img': 'image', 'video': 'video'},  # Name of the item to recover
                'video_cut': [0, None],  # Part of the video to study (in seconds)
                'fps': 0.1,  # Sampling frequency (for video file)
                'augmentation': True,  # Perform data augmentation on input data
                'balancing': True,
                'cropping': {'x_min': 770, 'x_max': 1170, 'y_min': 200, 'y_max': 885},
                'overwrite': False,
                'splitting_config': (5,),  # Set to False to avoid splitting
            },
            'use': {
                'overwrite': False,
                'name': 'labels',
                'save_img': False,
                'best_weights': True
            }
        }
    },

    # Data to process
    'source': 'test',
    'item_name': {'folder': 'famille', 'img': 'image', 'video': 'video'},  # Name of the item to recover
    #'suffix': '',  # Suffix to add to input & output items names
    'source_type': None,  # Type of item to recover (extension for a file, None for a folder)
    'video_cut': [0, None],  # Part of the video to study (in seconds)
    'fps': 0.1,  # Sampling frequency (for video file)

    # 'cropping': {'x_min': None, 'x_max': None, 'y_min': None, 'y_max': None},
    'cropping': {'x_min': 770, 'x_max': 1170, 'y_min': 200, 'y_max': 885},
    'shapes': {'shape1': {'import_mask': False, 'lower': (333, 4, 59), 'upper': (335, 6, 61)},},

    # Analysis parameters
    'detection_margin': {'left': 0, 'right': 0, 'top': 0, 'bottom': 0},
    'analysis_padding': {'one': {'left': 200, 'top': 200, 'right': 200, 'bottom': 200},
                         'multiple': {'left': 400, 'top': 400, 'right': 400, 'bottom': 400}},
    'debug': -2,
    'verbose': 1,

    # Output parameters
    'output': {
        'csv': {'activate': False, 'overwrite': True, 'name': 'data'},
        'img': {'activate': True, 'overwrite': True, 'name': 'frames_wLabels'},
        'video': {'activate': True, 'overwrite': False, 'name': 'video_wLabels'}
    },
    'complete_data': False,
    'speed': 1
}
